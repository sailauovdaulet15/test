<?php


namespace App\Services;


use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryServices
{
    public function store(array $data): Model
    {
        return Category::query()->create($data);
    }

}
