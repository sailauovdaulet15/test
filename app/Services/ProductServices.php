<?php


namespace App\Services;


use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductServices
{
    public function store(array $data)
    {
        DB::beginTransaction();
        try {
            $product = Product::query()->create($data);
            $product->categories()->attach($data['categories']);
            DB::commit();
            return $product;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getProducts(): Collection
    {
        $productName = request()->get('product_name');
        $categoryId = request()->get('category_id');

        return Product::query()
            ->where(function ($query) use ($productName, $categoryId) {
                if (!empty($productName)) {
                    $query->where('name', $productName);
                }
                if (!empty($categoryId)) {
                    $query->whereHas('categories', function ($q) use ($categoryId) {
                        $q->where('category_id', '=', $categoryId);
                    });
                }
            })
            ->get();

    }

}
