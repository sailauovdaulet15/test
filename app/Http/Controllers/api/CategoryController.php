<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\CategoryServices;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    private CategoryServices $categoryServices;

    public function __construct(CategoryServices $categoryServices)
    {
        $this->categoryServices = $categoryServices;
    }

    public function store(CategoryStoreRequest $request): CategoryResource
    {
        $data = $request->validated();
        return new CategoryResource($this->categoryServices->store($data));
    }

    public function destroy(Category $category): Response
    {
       $category->delete();
       return response(null, Response::HTTP_NO_CONTENT);
    }
}
