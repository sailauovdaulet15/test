<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\ProductServices;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    private ProductServices $productServices;

    public function __construct(ProductServices $productServices)
    {
        $this->productServices = $productServices;
    }

    public function index(): ResourceCollection
    {
        return ProductResource::collection($this->productServices->getProducts());
    }

    public function store(ProductRequest $request): ProductResource
    {
        $data = $request->validated();
        return new ProductResource($this->productServices->store($data));
    }

    public function destroy(Product $product): Response
    {
        $product->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function update(ProductUpdateRequest $request, Product $product): ProductResource
    {
        $data = $request->validated();
        $product->update($data);
        return new ProductResource($product);
    }
}
